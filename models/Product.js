const mongoose = require("mongoose");
const idValidator = require("mongoose-id-validator");

const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        min: [0, 'Price cannot be less than 0'],
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image: String,
    category: {
        type: Schema.Types.ObjectId,
        ref: "Category",
        require: true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    }
});

ProductSchema.plugin(idValidator, {
    message: 'Bad ID value for {PATH}'
});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;