const router = require("express").Router();
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");
const config = require("../config");
const Product = require("../models/Product");
const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
    let products;
    try {
        if(req.query.category) {
            products = await Product.find({category: req.query.category}).populate('user');
        } else {
            products = await Product.find().populate('user');
        }
        return res.send(products);
    } catch (e) {
        return res.status(500).send({error: 'Error'});
    }
});

router.get("/:id", async (req, res) => {
    const result = await Product.findById(req.params.id);
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});
router.post("/",auth, upload.single("image"), async (req, res) => {
    const productData = req.body;
    if (req.file) {
        productData.image = req.file.filename;
    }
    productData.user = req.user._id;
    const product = new Product(productData);
    try {
        await product.save();
        res.send(product);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.delete("/:id",auth, async (req, res) => {
    try {
        req.body.user = req.user._id;
        await Product.findOneAndRemove({ user: req.user._id, _id: req.params.id});
             return res.send({message: `Product was deleted`});
} catch (e) {
    return res.status(400).send({error: e});
}
});

module.exports = router;
