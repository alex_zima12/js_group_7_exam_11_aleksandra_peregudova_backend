const router = require("express").Router();
const Category = require("../models/Category");

router.get("/", async (req, res) => {
    try {
        const categories = await Category.find();
        res.send(categories);
    } catch (e) {
        res.sendStatus(500);
    }
});

module.exports = router;