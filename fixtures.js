const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const config = require("./config");

const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

mongoose.connect(config.db.url + "/" + config.db.name, {useNewUrlParser: true});

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("categories");
        await db.dropCollection("products");
        await db.dropCollection("users");
    } catch (e) {
        console.log("Collection were not presented, skipping drop...");
    }

    const [auto, clothes, animals] = await Category.create({
        title: 'auto'
    },
        {
        title: 'clothes'
    },
        {
        title: 'animals'
    });

    const [user, user2] =await User.create({
        username: "user",
        password: "1@345qWert",
        displayName: "User",
        phone:"0551234578",
        token: nanoid()
    }, {
        username: "user2",
        password: "1@345qWert",
        displayName: "User2",
        phone:"055123457",
        token: nanoid()
    });

    await Product.create({
        title: 'Toyota',
        price: '75555',
        category: auto._id,
        user: user2._id,
        description: 'Продаю Toyota ',
        image: 'kYepwAGijDNsJQh1lMJ1V.jpg'
    }, {
        title: 'Mercedes Benz',
        price: '1525500',
        category: auto._id,
        user: user._id,
        description: 'Продаю Mercedes',
        image: 'FFduHjrnuSGBRLOE-gzRo.jpeg'
    }, {
        title: 'Юбка',
        price: '200',
        category: clothes._id,
        user: user._id,
        description: 'Продаю юбку',
        image: 'gk3MVd2SXOi6LQl0Gq8oP.jpg'
    }, {
        title: 'Кот',
        price: '900',
        category: animals._id,
        user: user._id,
        description: 'Продаю кота',
        image: 'rjktmaXqqppzToERReIr2.jpg'
    });

    db.close();
});